package quadros.julio.desafio.model;

import android.os.Parcel;
import android.os.Parcelable;

import static android.R.attr.id;

/**
 * Created by julioquadros on 31/08/16.
 */
public class PullRequest implements Parcelable {
    private String html_url;
    private String id;
    private String title;
    private Owner user;
    private String body;

    public PullRequest(String html_url, String id, String title, Owner user, String body) {
        this.html_url = html_url;
        this.id = id;
        this.title = title;
        this.user = user;
        this.body = body;
    }

    protected PullRequest(Parcel in) {
        html_url = in.readString();
        id = in.readString();
        title = in.readString();
        user = in.readParcelable(Owner.class.getClassLoader());
        body = in.readString();
    }

    public static final Creator<PullRequest> CREATOR = new Creator<PullRequest>() {
        @Override
        public PullRequest createFromParcel(Parcel in) {
            return new PullRequest(in);
        }

        @Override
        public PullRequest[] newArray(int size) {
            return new PullRequest[size];
        }
    };

    public String getHtml_url() {
        return html_url;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Owner getUser() {
        return user;
    }

    public String getBody() {
        return body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeParcelable(user, PARCELABLE_WRITE_RETURN_VALUE);
        dest.writeString(html_url);
        dest.writeString(title);
        dest.writeString(body);
    }
}

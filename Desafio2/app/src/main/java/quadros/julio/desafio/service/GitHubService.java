package quadros.julio.desafio.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by julioquadros on 31/08/16.
 */
public interface GitHubService {
    @GET("search/repositories?q=language:Java")
    Call<JsonObject> listRepos(@Query("sort") String sortParameter, @Query("page") int page);

    @GET("https://api.github.com/repos/{owner}/{repository}/pulls")
    Call<JsonArray> listPulls(@Path("owner") String owner, @Path("repository") String repository);
}

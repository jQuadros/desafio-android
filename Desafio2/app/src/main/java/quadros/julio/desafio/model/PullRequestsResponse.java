package quadros.julio.desafio.model;

import java.util.List;

/**
 * Created by julioquadros on 31/08/16.
 */
public class PullRequestsResponse {

    private final List<PullRequest> items;

    public PullRequestsResponse(List<PullRequest> items) {
        this.items = items;
    }

    public List<PullRequest> getItems() {
        return items;
    }
}

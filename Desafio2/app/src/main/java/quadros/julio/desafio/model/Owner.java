package quadros.julio.desafio.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by julioquadros on 31/08/16.
 */
public class Owner implements Parcelable{

    private final long id;
    private final String login;
    private final String avatar_url;

    public Owner(long id, String login, String avatar_url) {
        this.id = id;
        this.login = login;
        this.avatar_url = avatar_url;
    }

    protected Owner(Parcel in) {
        id = in.readLong();
        login = in.readString();
        avatar_url = in.readString();
    }

    public static final Creator<Owner> CREATOR = new Creator<Owner>() {
        @Override
        public Owner createFromParcel(Parcel in) {
            return new Owner(in);
        }

        @Override
        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };

    public long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getByLogin() {
        return "By " + login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(login);
        dest.writeString(avatar_url);
    }
}

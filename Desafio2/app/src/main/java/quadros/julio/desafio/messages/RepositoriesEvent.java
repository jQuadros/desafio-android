package quadros.julio.desafio.messages;

import quadros.julio.desafio.model.RepositoriesResponse;

/**
 * Created by julioquadros on 31/08/16.
 */
public class RepositoriesEvent {

    public RepositoriesResponse response;

    public RepositoriesEvent(RepositoriesResponse response) {
        this.response = response;
    }
}

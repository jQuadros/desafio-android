package quadros.julio.desafio.model;

import java.util.List;

/**
 * Created by julioquadros on 31/08/16.
 */
public class RepositoriesResponse {

    private final long total_count;
    private final boolean incomplete_results;
    private final List<Repository> items;

    public RepositoriesResponse(long total_count, boolean incomplete_results, List<Repository> items) {
        this.total_count = total_count;
        this.incomplete_results = incomplete_results;
        this.items = items;
    }

    public long getTotal_count() {
        return total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public List<Repository> getItems() {
        return items;
    }
}

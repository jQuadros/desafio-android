package quadros.julio.desafio.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import quadros.julio.desafio.R;
import quadros.julio.desafio.adapters.PullRequestsAdapter;
import quadros.julio.desafio.messages.PullRequestEvent;
import quadros.julio.desafio.model.PullRequest;
import quadros.julio.desafio.service.CallAPI;
import quadros.julio.desafio.utils.Converter;
import quadros.julio.desafio.utils.RecyclerItemClickListener;

public class PullRequestsActivity extends AppCompatActivity {

    PullRequestsAdapter pullAdapter;
    RecyclerView recyclerPulls;
    RecyclerView.LayoutManager recyclerPullsLayoutManager;

    AVLoadingIndicatorView loadingView;

    ImageView imgOwner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulls);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarPulls);
        setSupportActionBar(toolbar);

        configureWidgets();
        if(savedInstanceState == null) {
            getIntentExtrasAndCallAPI();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        restoreState(savedInstanceState);
    }

    private void configureWidgets() {

        imgOwner = (ImageView) findViewById(R.id.imgOwner);
        loadingView = (AVLoadingIndicatorView) findViewById(R.id.loadingView);

        pullAdapter = new PullRequestsAdapter(new ArrayList<PullRequest>());
        recyclerPullsLayoutManager = new LinearLayoutManager(this);

        recyclerPulls = (RecyclerView) findViewById(R.id.recyclerPulls);
        recyclerPulls.setHasFixedSize(true);
        recyclerPulls.setLayoutManager(recyclerPullsLayoutManager);
        recyclerPulls.setAdapter(pullAdapter);

        recyclerPulls.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recyclerPulls ,new RecyclerItemClickListener.OnItemClickListener() {

                    AlertDialog.Builder alert = new AlertDialog.Builder(PullRequestsActivity.this);

                    @Override

                    public void onItemClick(View view, int position) {

                        PullRequest pullRequest = pullAdapter.getItem(position);
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(pullRequest.getHtml_url())));

                    }

                    @Override
                    public void onLongItemClick(View view, int position, MotionEvent e) {

                        PullRequest pullRequest = ((PullRequestsAdapter) pullAdapter).getItem(position);

                        alert.setTitle(pullRequest.getTitle());
                        alert.setMessage(pullRequest.getBody());
                        alert.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alert.show();
                    }

                    @Override
                    public void onLongClickUp() {
                    }
                })
        );

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PullRequestEvent event) {
        if(event.response != null) {
            populateList(event.response);
        }
    }

    private void populateList(List<PullRequest> pullRequests) {
        for(int i = 0; i < pullRequests.size(); i ++) {
            ((PullRequestsAdapter) pullAdapter).addItem(pullRequests.get(i), i);
        }
        hideLoading();
    }

    private void getIntentExtrasAndCallAPI() {
        Bundle bundle = getIntent().getExtras();
        byte[] imageBytes = (byte[]) bundle.get("toolbarImage");
        String ownerName = bundle.get("ownerName").toString();
        String repositoryName = bundle.get("repositoryName").toString();

        setToolbarImage(imageBytes);

        callAPI(ownerName, repositoryName);
    }

    private void setToolbarImage(byte[] imageBytes) {
        Drawable toolbarImage = Converter.fromBytes(imageBytes, PullRequestsActivity.this);
        imgOwner.setImageDrawable(toolbarImage);
    }

    private void callAPI(String ownerName, String repositoryName) {
        new CallAPI().getPullRequests(ownerName, repositoryName);
        loadingView.show();
    }

    private void saveState(Bundle outState) {
        outState.putParcelableArrayList("pullrequests", (ArrayList<? extends Parcelable>) pullAdapter.getItems());
        Drawable ownerImage = ((ImageView) imgOwner).getDrawable();
        outState.putByteArray("toolbarImage", Converter.fromDrawable(ownerImage));
    }

    private void restoreState(Bundle savedInstanceState) {
        List<PullRequest> pullRequests = savedInstanceState.<PullRequest>getParcelableArrayList("pullrequests");
        pullAdapter.setItems(pullRequests);

        byte[] imageBytes = (byte[]) savedInstanceState.get("toolbarImage");
        setToolbarImage(imageBytes);

        hideLoading();
    }

    private void hideLoading() {
        loadingView.hide();
    }
}

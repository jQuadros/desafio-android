package quadros.julio.desafio.adapters;

/**
 * Created by julioquadros on 01/09/16.
 */

import android.databinding.BindingAdapter;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import quadros.julio.desafio.R;

public class DataBindingAdatpers {

    @BindingAdapter("bind:imageUrl")
    public static void loadImage(final ImageView view, final String url) {
        Picasso.with(view.getContext()).load(url).into(view);
        Picasso.with(view.getContext())
            .load(url)
            .networkPolicy(NetworkPolicy.OFFLINE)
            .into(view, new Callback() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError() {
                    //Try again online if cache failed
                    Picasso.with(view.getContext())
                        .load(url)
                        .into(view, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Log.v("Picasso","Could not fetch image");
                            }
                        });
                    }
                });
    }

}

package quadros.julio.desafio.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import quadros.julio.desafio.R;
import quadros.julio.desafio.model.PullRequest;

/**
 * Created by julioquadros on 31/08/16.
 */
public class PullRequestsAdapter extends RecyclerView.Adapter<AdapterHolder> {

    private List<PullRequest> pullRequests;

    public PullRequestsAdapter(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }

    @Override
    public AdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_pullrequest_item, parent, false);

        AdapterHolder dataObjectHolder = new AdapterHolder(view);

        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(AdapterHolder holder, int position) {

        final PullRequest pullRequest= pullRequests.get(position);
        holder.getBinding().setVariable(quadros.julio.desafio.BR.pullrequest, pullRequest);
        holder.getBinding().executePendingBindings();

    }

    public void addItem(PullRequest dataObj, int index) {
        pullRequests.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void addItems(List<PullRequest> newPullRequests) {
        int lastPosition = pullRequests.size();
        pullRequests.addAll(newPullRequests);
        notifyItemRangeInserted(lastPosition, pullRequests.size());
    }

    public void deleteItem(int index) {
        pullRequests.remove(index);
        notifyItemRemoved(index);
    }

    public PullRequest getItem(int position) {
        return pullRequests.get(position);
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public List<PullRequest> getItems() {
        return pullRequests;
    }

    public void setItems(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }
}

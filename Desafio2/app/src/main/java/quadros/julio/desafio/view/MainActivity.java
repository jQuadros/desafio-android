package quadros.julio.desafio.view;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import quadros.julio.desafio.R;
import quadros.julio.desafio.adapters.RepositoriesAdapter;
import quadros.julio.desafio.messages.RepositoriesEvent;
import quadros.julio.desafio.model.Repository;
import quadros.julio.desafio.service.CallAPI;
import quadros.julio.desafio.utils.ButtonAction;
import quadros.julio.desafio.utils.CheckConnection;
import quadros.julio.desafio.utils.Converter;
import quadros.julio.desafio.utils.RecyclerItemClickListener;

public class MainActivity extends AppCompatActivity {

    private TextView txNoData;
    private CoordinatorLayout coordinatorLayout;
    private AVLoadingIndicatorView loadingView;

    private RecyclerView mRecyclerView;
    private RepositoriesAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    private boolean LOADING = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int PAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.home_toolbar);
        setSupportActionBar(myToolbar);

        configureWidgets();

        if(savedInstanceState == null) {
            callAPI(PAGE);
            showGuide();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        restoreState(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        callAPI(PAGE);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RepositoriesEvent event) {
        if(event.response != null) {
            populateList(event.response.getItems());
        }
        else {

            boolean isConnected = CheckConnection.isConnected(this);

            if(!isConnected) {
                showWrongMessage("Enable internet.", "Settings", ButtonAction.SETTINGS);
            }
            else {
                showWrongMessage("No data to show.", "Retry", ButtonAction.RETRY);
            }

            hideLoading();
        }
    }

    private void configureWidgets() {

        txNoData = (TextView) findViewById(R.id.txNoData);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorMain);
        loadingView = (AVLoadingIndicatorView) findViewById(R.id.loadingView);

        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new RepositoriesAdapter(new ArrayList<Repository>(), this);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0)
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (LOADING)
                    {
                        // Retorna mais diretórios caso o usuário tenha passado por 2/3 da lista.
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount - (totalItemCount/3))
                        {
                            LOADING = false;
                            callAPI(++PAGE);
                        }
                    }
                }
            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, mRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {

                    Dialog dialog = new Dialog(MainActivity.this);

                    @Override
                    public void onItemClick(View view, int position) {
                        Repository repository = mAdapter.getItem(position);

                        View sharedImageView = (ImageView) view.findViewById(R.id.imageView);
                        View sharedTextView = (TextView) view.findViewById(R.id.textView);
                        String transitionImageName = getString(R.string.ownerImage_transition);
                        String transitionTextName = getString(R.string.repositoryName_transition);

                        Drawable ownerImage = ((ImageView) sharedImageView).getDrawable();

                        Intent intent = new Intent(MainActivity.this, PullRequestsActivity.class);
                        intent.putExtra("toolbarImage", Converter.fromDrawable(ownerImage));
                        intent.putExtra("ownerName", repository.getOwner().getLogin());
                        intent.putExtra("repositoryName", repository.getName());

                        ActivityOptions trasitionOptions = ActivityOptions.makeSceneTransitionAnimation(
                                MainActivity.this,
                                android.util.Pair.create(sharedImageView, transitionImageName),
                                android.util.Pair.create(sharedTextView, transitionTextName));

                        startActivity(intent, trasitionOptions.toBundle());

                    }

                    @Override
                    public void onLongItemClick(View view, int position, MotionEvent e) {

                        Repository repository = ((RepositoriesAdapter) mAdapter).getItem(position);

                        dialog.setContentView(R.layout.repository_alert_info);
                        TextView txDescription = (TextView) dialog.findViewById(R.id.txDescription);
                        txDescription.setText(repository.getDescription());
                        dialog.show();

                    }

                    @Override
                    public void onLongClickUp() {
                        dialog.dismiss();
                    }
                })
        );

    }

    private void populateList(List<Repository> response) {
        hideNoDataText();
        mRecyclerView.setVisibility(View.VISIBLE);

        mAdapter.addItems(response);
        LOADING = true;
        hideLoading();
    }

    private void showGuide() {
        AlertDialog.Builder guide = new AlertDialog.Builder(this);
        guide.setTitle("Bem vindo!");
        guide.setMessage("Clique nos cards para mostrar mais dados. \nUm clique longo mostra a descrição.");
        guide.setNeutralButton("Entendi", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        guide.show();
    }

    private void showWrongMessage(String message, String buttonText, final ButtonAction action) {
        mRecyclerView.setVisibility(View.GONE);
        txNoData.setVisibility(View.VISIBLE);

        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_INDEFINITE)
            .setAction(buttonText, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                if(action == ButtonAction.RETRY) {
                    callAPI(PAGE);
                }
                else {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                }
                }
            })
            .setActionTextColor(Color.RED)
            .show();
    }

    private void saveState(Bundle outState) {
        outState.putParcelableArrayList("repositories", (ArrayList<? extends Parcelable>) mAdapter.getItems());
    }

    private void restoreState(Bundle savedInstanceState) {
        List<Repository> repositories = savedInstanceState.<Repository>getParcelableArrayList("repositories");
        mAdapter.setItems(repositories);
        hideLoading();
    }

    private void hideLoading() {
        loadingView.hide();
        hideNoDataText();
    }

    private void showLoading() {
        loadingView.show();
    }

    private void callAPI(int PAGE) {
        new CallAPI().getRepositories(PAGE);
        showLoading();
    }

    private void hideNoDataText() {
        txNoData.setVisibility(View.GONE);
    }
}

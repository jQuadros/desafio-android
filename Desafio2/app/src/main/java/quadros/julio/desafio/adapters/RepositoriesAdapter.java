package quadros.julio.desafio.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import quadros.julio.desafio.R;
import quadros.julio.desafio.model.Repository;

/**
 * Created by julioquadros on 31/08/16.
 */
public class RepositoriesAdapter extends RecyclerView.Adapter<AdapterHolder> {

    private List<Repository> repositories;
    private Context context;

    public RepositoriesAdapter(List<Repository> repositories, Context context) {
        this.repositories = repositories;
        this.context = context;
    }

    @Override
    public AdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_repository_item, parent, false);

        TextView txStarsSimbol = (TextView) view.findViewById(R.id.txStarsSimbol);
        TextView txForkSimbol = (TextView) view.findViewById(R.id.txForkSimbol);
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/fontawesome-webfont.ttf");
        txStarsSimbol.setTypeface(tf);
        txForkSimbol.setTypeface(tf);

        AdapterHolder dataObjectHolder = new AdapterHolder(view);

        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(AdapterHolder holder, int position) {

        final Repository repository = repositories.get(position);
        holder.getBinding().setVariable(quadros.julio.desafio.BR.repository, repository);
        holder.getBinding().executePendingBindings();

    }

    public void addItem(Repository dataObj, int index) {
        repositories.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void addItems(List<Repository> newRepositories) {
        int lastPosition = repositories.size();
        repositories.addAll(newRepositories);
        notifyItemRangeInserted(lastPosition, repositories.size());
    }

    public void deleteItem(int index) {
        repositories.remove(index);
        notifyItemRemoved(index);
    }

    public List<Repository> getItems() {
        return this.repositories;
    }

    public void setItems(List<Repository> repositories) {
        this.repositories = repositories;
        notifyDataSetChanged();
    }

    public Repository getItem(int position) {
        return repositories.get(position);
    }

    @Override
    public int getItemCount() {
        return this.repositories.size();
    }

}

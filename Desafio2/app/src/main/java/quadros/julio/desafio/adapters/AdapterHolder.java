package quadros.julio.desafio.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by julioquadros on 01/09/16.
 */

public class AdapterHolder extends RecyclerView.ViewHolder {
    private ViewDataBinding binding;

    public AdapterHolder(View rowView) {
        super(rowView);
        binding = DataBindingUtil.bind(rowView);
    }
    public ViewDataBinding getBinding() {
        return binding;
    }
}
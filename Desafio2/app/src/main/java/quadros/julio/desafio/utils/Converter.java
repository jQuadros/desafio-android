package quadros.julio.desafio.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.io.ByteArrayOutputStream;

/**
 * Created by julioquadros on 02/09/16.
 */

public class Converter {

    public static byte[] fromDrawable(Drawable image) {
        Bitmap bitmap = ((BitmapDrawable) image).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    public static Drawable fromBytes(byte[] image, Context context) {
        return new BitmapDrawable(context.getResources(), BitmapFactory.decodeByteArray(image, 0, image.length));
    }

}

package quadros.julio.desafio.messages;

import java.util.List;

import quadros.julio.desafio.model.PullRequest;

/**
 * Created by julioquadros on 31/08/16.
 */
public class PullRequestEvent {

    public List<PullRequest> response;

    public PullRequestEvent(List<PullRequest> response) {
        this.response = response;
    }
}

package quadros.julio.desafio.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import quadros.julio.desafio.messages.PullRequestEvent;
import quadros.julio.desafio.messages.RepositoriesEvent;
import quadros.julio.desafio.model.PullRequest;
import quadros.julio.desafio.model.PullRequestsResponse;
import quadros.julio.desafio.model.RepositoriesResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by julioquadros on 31/08/16.
 */
public class CallAPI {

    static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.github.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public void getRepositories(int page) {
        GitHubService service = retrofit.create(GitHubService.class);

        Call<JsonObject> repositories = service.listRepos("stars", page);
        repositories.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                int code = response.code();

                if(code == 200) {
                    JsonObject responseAsArray = response.body().getAsJsonObject();
                    RepositoriesEvent event = new RepositoriesEvent(
                            new Gson().fromJson(responseAsArray, RepositoriesResponse.class));
                    EventBus.getDefault().post(event);
                }
                else {
                    sendEmptyResponse();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                sendEmptyResponse();
            }

            private void sendEmptyResponse() {
                EventBus.getDefault().post(new RepositoriesEvent(null));
            }

        });
    }

    public void getPullRequests(String owner, String repository) {
        GitHubService service = retrofit.create(GitHubService.class);

        Call<JsonArray> pulls = service.listPulls(owner, repository);
        pulls.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                if(response.code() == 200) {
                    JsonArray responseArray = response.body().getAsJsonArray();

                    List<PullRequest> pullRequests = new ArrayList<PullRequest>();
                    Gson gson = new Gson();

                    for (JsonElement element : responseArray) {
                        pullRequests.add(gson.fromJson(element, PullRequest.class));
                    }

                    PullRequestEvent event = new PullRequestEvent(pullRequests);
                    EventBus.getDefault().post(event);
                }
                else {
                    sendEmptyResponse();
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                sendEmptyResponse();
            }

            private void sendEmptyResponse() {
                EventBus.getDefault().post(new PullRequestsResponse(null));
            }
        });
    }
}

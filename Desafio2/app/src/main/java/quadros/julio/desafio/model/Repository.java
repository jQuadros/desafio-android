package quadros.julio.desafio.model;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by julioquadros on 31/08/16.
 */
public class Repository implements Parcelable {

    private final long id;
    private final String name;
    private final String full_name;
    private final Owner owner;
    private final String html_url;
    private final String description;
    private final String forks_url;
    private final String statuses_url;
    private final int stargazers_count;
    private final int forks;

    public Repository(long id, String name, String full_name, Owner owner, String html_url, String description, String forks_url, String statuses_url, int stargazers_count, int forks) {
        this.id = id;
        this.name = name;
        this.full_name = full_name;
        this.owner = owner;
        this.html_url = html_url;
        this.description = description;
        this.forks_url = forks_url;
        this.statuses_url = statuses_url;
        this.stargazers_count = stargazers_count;
        this.forks = forks;
    }

    public Repository(Parcel in) {

        id = in.readInt();
        name = in.readString();
        full_name = in.readString();
        owner = in.readParcelable(Owner.class.getClassLoader());
        html_url = in.readString();
        description = in.readString();
        forks_url = in.readString();
        statuses_url = in.readString();
        stargazers_count = in.readInt();
        forks = in.readInt();
    }

    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFull_name() {
        return full_name;
    }

    public Owner getOwner() {
        return owner;
    }

    public String getHtml_url() {
        return html_url;
    }

    public String getDescription() {
        return description;
    }

    public String getForks_url() {
        return forks_url;
    }

    public String getStatuses_url() {
        return statuses_url;
    }

    public int getStargazers_count() {
        return stargazers_count;
    }

    public int getForks() {
        return forks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(full_name);
        dest.writeParcelable(owner, PARCELABLE_WRITE_RETURN_VALUE);
        dest.writeString(html_url);
        dest.writeString(description);
        dest.writeString(forks_url);
        dest.writeString(statuses_url);
        dest.writeInt(stargazers_count);
        dest.writeInt(forks);
    }
}
